
// Logos by Dustin Howett
// See http://iphonedevwiki.net/index.php/Logos
//@interface NSFileManager : NSObject
//- (bool)fileExistsAtPath:(id)arg1;
//- (bool)fileExistsAtPath:(id)arg1 isDirectory:(bool*)arg2;
//@end
#import <mach-o/dyld.h>
#import <Foundation/NSFileManager.h>
// Logos by Dustin Howett
// See http://iphonedevwiki.net/index.php/Logos

pid_t fork(void);
%hookf(pid_t, fork) {
    %orig;
    return -1;
    
}
//const char * _dyld_get_image_name(uint32_t image_index);
//%hookf(const char *, _dyld_get_image_name, uint32_t image_index) {
//    
//    const char *original = %orig;
//    if (strstr(original, "MobileSubstrate") != NULL) {
//        return "";
//    } else {
//        return original;
//    }
//}


%group EXEC_GROUP
int system(const char *command);
%hookf(int, system, const char *command) {
    if (strstr(command, NULL)) {
        return 0;
    } else {
        if (strstr(command, nil)) {
            return 0;
        } else {
            if (strstr(command, "")) {
                return 0;
            } else {
                return %orig;
            }
        }
        
    }
}
#pragma mark -
#pragma mark NSFileManager


%hook NSFileManager


- (BOOL)fileExistsAtPath:(id)arg1{
    %log;
    if ([arg1 isEqualToString:@"/bin/bash"]) {
        return NO;
    } else {
        if ([arg1 isEqualToString:@"/bin/sh"]) {
            return NO;
        } else {
            if ([arg1 isEqualToString:@"/Applications/Cydia.app"]) {
                return NO;
            } else {
                if ([arg1 isEqualToString:@"/Applications/SpringBoard.app"]) {
                    return NO;
                } else {
                    if ([arg1 isEqualToString:@"/usr/sbin/sshd"]) {
                        return NO;
                    } else {
                        if ([arg1 isEqualToString:@"/etc/apt"]) {
                            return NO;
                        } else {
                            if ([arg1 isEqualToString:@"/Library/MobileSubstrate/MobileSubstrate.dylib"]) {
                                return NO;
                            } else {
                                if ([arg1 isEqualToString:@"/usr/bin/ssh"]) {
                                    return NO;
                                    
                                } else {
                                    if ([arg1 isEqualToString:@"/private/var/lib/apt"]) {
                                        return NO;
                                    } else {
                                    
                                    
                                    
                                    %orig(arg1);
                                }
                                }
                                
                            }
                        }
                        
                        return false;
                    }
                }
            }
        }
    }
}
                    - (BOOL)fileExistsAtPath:(id)arg1 isDirectory:(bool*)arg2 {
                        %log;
                        if ([arg1 isEqualToString:@"/bin/bash"]) {
                            return NO;
                        } else {
                            if ([arg1 isEqualToString:@"/bin/sh"]) {
                                return NO;
                            } else {
                                if ([arg1 isEqualToString:@"/Applications/Cydia.app"]) {
                                    return NO;
                                } else {
                                    if ([arg1 isEqualToString:@"/Applications/SpringBoard.app"]) {
                                        return NO;
                                    } else {
                                        if ([arg1 isEqualToString:@"/usr/sbin/sshd"]) {
                                            return NO;
                                        } else {
                                            if ([arg1 isEqualToString:@"/etc/apt"]) {
                                                return NO;
                                            } else {
                                                if ([arg1 isEqualToString:@"/Library/MobileSubstrate/MobileSubstrate.dylib"]) {
                                                    return NO;
                                                } else {
                                                    if ([arg1 isEqualToString:@"/usr/bin/ssh"]) {
                                                        return NO;
                                                     
                                                    } else {
                                                    if ([arg1 isEqualToString:@"/private/var/lib/apt"]) {
                                                    return NO;
                                                    }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            return %orig;
                        }
                    }
                    
                    %end
                    %hook NPFCapabilities
                    - (_Bool)isSandbox {
                        return true;
                    }
                    %end
                    
                    %hook NPFSDK
                    + (_Bool)isSandbox {
                        return true;
                    }
                    %end
                    %hook FIRInstanceID
                    - (_Bool)isSandboxApp {
                        return true;
                    }
                    %end
                    %hook FIRInstanceIDStore
                    
                    %end
                    %hook NSString
                    - (BOOL)writeToFile:(id)arg1 atomically:(BOOL)arg2 {
                        return false;
                    }
                    - (BOOL) writeToFile:(id)arg1 atomically:(BOOL)arg2 encoding:(unsigned int)arg3 error:(id*)arg4 {
                        return false;
                    }
                    %end
                    %hookf(FILE *, fopen, const char *path, const char *mode) {
                        if (strcmp(path, "/Applications/Cydia.app")) {
                            return NULL;
                        } else {
                            if (strcmp(path, "/Library/MobileSubstrate/MobileSubstrate.dylib")) {
                                return NULL;
                            } else {
                                if (strcmp(path, "/bin/bash")) {
                                    return NULL;
                                } else {
                                    if (strcmp(path, "/usr/sbin/sshd")) {
                                        return NULL;
                                    } else {
                                        if (strcmp(path, "/etc/apt")) {
                                            return NULL;
                                        } else {
                                            if (strcmp(path, "/usr/bin/ssh")) {
                                                return NULL;
                                            } else {
                                                if (strcmp(path, "/private/var/lib/apt")) {
                                                    return NULL;
                                                } else {
                                                return %orig;    
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // Call the original implementation of this function
                    }
%end

%ctor {
    
    NSString *bundleID = [[NSBundle mainBundle] bundleIdentifier];
    NSString *plistpath = @"";
    NSMutableDictionary *loadDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistpath];
    if ([[loadDict objectForKey:bundleID] boolValue]) {
        %init(EXEC_GROUP);
    }
}
